﻿using System;

namespace WindGUI
{
    class Averager
    {
        //static void Main()  // Tests the Sampler function
        //{
        //    double[] test = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
        //    string testType = "test";

        //    double[] sampledArray = Sampler(test, testType);

        //    for (int i = 0; i < sampledArray.Length; i++)
        //    {
        //        Console.Write("{0} ", sampledArray[i]);
        //    }
        //}
        public double[,] Sampler(double[] dataSet, string zoom)
        {
            int sample = 0;
            int pointNum = 0;
            double sum = 0;
            double average = 0;
            int length = dataSet.Length;

            switch (zoom)   // This clarifies the sample time
            {
                case "minute":
                    sample = 60;
                    break;
                case "hour":
                    sample = 3600;
                    break;
                case "day":
                    sample = 86400;
                    break;
                default:
                    break;
            }

            double[,] outputArray = new double[(int)Math.Ceiling((decimal)length / sample),2];

            int numSamples = 0;
            for (int i = 0; i <= length / sample; i++)   // This does the averaging
            {
                for (int y = pointNum * sample; y < (pointNum + 1) * sample; y++)
                {
                    if (y >= dataSet.Length - 1)
                    {
                        break;
                    }
                    sum = (double)(sum + dataSet[y]);
                    numSamples++;

                }
                
                average = (double)(sum / numSamples);
                if (i != 0)
                {
                    outputArray[i, 0] = (double)i - (double)1 + (double)numSamples / (double)sample;
                    outputArray[i, 1] = average;
                }
                else
                {
                    outputArray[i, 0] = 0;
                    outputArray[i, 1] = average;
                }
                sum = 0;
                pointNum++;
                numSamples = 0;
            }

            return outputArray;
        }
    }
}
