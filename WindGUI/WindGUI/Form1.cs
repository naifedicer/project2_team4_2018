﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindGUI
{

    public partial class Form1 : Form
    {
        FolderBrowserDialog browser = new FolderBrowserDialog();
        string windyDataFolder = "";
        Parse parse1 = new Parse();
        Averager average1 = new Averager();
        double[,] rawdata;
        List<double> timelist = new List<double>();
        List<double> speedlist = new List<double>();
        List<double> YAveraged = new List<double>();
        double[,] Averaged;
        List<double> XAveraged = new List<double>();
        string TabSelected = "day";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (browser.ShowDialog() == DialogResult.OK)
            {
                windyDataFolder = browser.SelectedPath; // prints path
                var windyFiles = from f in System.IO.Directory.GetFiles(windyDataFolder)
                                 where f.EndsWith(".h01")
                                 select f;

                foreach (var windyFile in windyFiles)
                {
                    rawdata = parse1.LineSplit(windyFile);

                    for (int j = 0; j < rawdata.GetLength(0); j++)
                    {
                        timelist.Add(rawdata[j, 0]);
                        speedlist.Add(rawdata[j, 1]);
                    }
                    //textBox1.AppendText(windyFile + "\n"); // for debugging, displays files

                    

                }
            }
            int averageSplit = 0;
            switch (TabSelected)   // This clarifies the sample time
            {
                case "minute":
                    averageSplit = 60;
                    break;
                case "hour":
                    averageSplit = 3600;
                    break;
                case "day":
                    averageSplit = 86400;
                    break;
                default:
                    break;
            }
            Averaged = average1.Sampler(speedlist.ToArray(), TabSelected);
            YAveraged.Clear();
            XAveraged.Clear();
            for (int k = 0; k < Averaged.GetLength(0); k++)
            {
                YAveraged.Add(Averaged[k, 1]);
                XAveraged.Add(Averaged[k, 0]);
            }
            //int i = 1;
            //while (i < timelist.Count)
            //{
            //    for (int count = 0; count < averageSplit-1; count++)
            //    {
            //        if (i < timelist.Count)
            //        {
            //            timelist.RemoveAt(i);
            //        }
            //    }
            //    i++;
            //}
            //XAveraged = timelist.ToArray();
            chart1.Series[0].Points.Clear();
            //chart1.Series[0].Points.DataBindY(YAveraged);
            chart1.Series[0].Points.DataBindXY(XAveraged,YAveraged);
            //chart1.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = false;
            //chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle = new System.Windows.Forms.DataVisualization.Charting.LabelStyle() { IntervalType = DateTimeIntervalType.Minutes, Interval = 10, Format = "HH:mm:ss" };
            //chart1.Series[0].Points.DataBindX(XAveraged);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void openFileDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Implement code to prompt for directory of wind data files
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            TabSelected = "minute";
            chart1.Series[0].Points.Clear();
            if (speedlist.Count > 0)
            {
                Averaged = average1.Sampler(speedlist.ToArray(), TabSelected);
                YAveraged.Clear();
                XAveraged.Clear();
                for (int k = 0; k < Averaged.GetLength(0); k++)
                {
                    YAveraged.Add(Averaged[k, 1]);
                    XAveraged.Add(Averaged[k, 0]);
                }
                chart1.Series[0].Points.Clear();
                //chart1.Series[0].Points.DataBindY(YAveraged);
                chart1.Series[0].Points.DataBindXY(XAveraged, YAveraged);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            TabSelected = "day";
            chart1.Series[0].Points.Clear();
            if (speedlist.Count > 0)
            {
                Averaged = average1.Sampler(speedlist.ToArray(), TabSelected);
                YAveraged.Clear();
                XAveraged.Clear();
                for (int k = 0; k < Averaged.GetLength(0); k++)
                {
                    YAveraged.Add(Averaged[k, 1]);
                    XAveraged.Add(Averaged[k, 0]);
                }
                chart1.Series[0].Points.Clear();
                //chart1.Series[0].Points.DataBindY(YAveraged);
                chart1.Series[0].Points.DataBindXY(XAveraged, YAveraged);

            }
        }
    }
}
