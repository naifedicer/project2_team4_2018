﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;


namespace WindGUI
{
    class Parse
    {
        public double[,] LineSplit(string fileDirectory)
        {
            string text = System.IO.File.ReadAllText(fileDirectory);
            text = text.Replace("\r", "");
            text = text.Replace(" ", "");
            string[] substrings = text.Split('\n');
            string[,] splitSubstrings = new string[substrings.Length, 2];
            string[] intermediate = new string[4];
            int k = 0;
            int skip = 0;
            foreach (var substring in substrings)
            {
                if (skip == 0) //skips column headers
                {
                    skip = 1;
                }
                else
                {
                    intermediate = substring.Split('\t');
                    if (intermediate[0] != "")
                    {
                        splitSubstrings[k, 0] = intermediate[0];
                        splitSubstrings[k, 1] = intermediate[1];
                    }
                    k++;
                }
            }

            double[,] data = new double[substrings.GetLength(0) - 1, 2];
            
            // Fills the data array with the wind speed values
            for (int i = 0; i < (substrings.GetLength(0) - 2); i++)
            {
                double number;
                double.TryParse(splitSubstrings[i + 1, 0], out number);
                data[i, 0] = number;
                double.TryParse(splitSubstrings[i + 1, 1], out number);
                data[i, 1] = number;
            }

            return data;

        }
        
    }
}
